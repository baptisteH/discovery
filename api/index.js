import spotify from './spotify'
import spotifyAuth from './spotifyAuth'

export default {
  spotify,
  spotifyAuth
}
