import axios from 'axios'

const token = 'BQCR7tTY3DFa1yTPZlMicDshAa_EVK2oyr1gSwOf4_0O2uSVZNT_rZaw5QN7MrMnXydWiIIOj1_nS_LPp_o'

const request = axios.create({
  baseURL: 'https://api.spotify.com/v1/',
  headers: {
    Authorization: `Bearer ${token}`
  }
})

export default request
