import request from './request'
import search from './search'
import tracks from './tracks'

export default {
  request,
  search,
  tracks
}
