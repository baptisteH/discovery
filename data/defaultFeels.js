export const defaultFeels = [
  {
    id: '0',
    userId: '0',
    discoveriesNumber: 0,
    track: {
      title: 'What Kinda Music',
      artistName: 'Tom Misch',
      coverImage: 'https://i.scdn.co/image/ab67616d00001e02bd93bbf0c3df5e0242b84831',
      previewUrl: 'https://p.scdn.co/mp3-preview/6ea10f0e7d19084fac3539e79ee66cec12d05c19?cid=774b29d4f13844c495f206cafdad9c86'
    },
    words: ['Canyon', 'Road Trip', 'Sauvage'],
    visual: {
      hasCover: true,
      color: '#000',
      colorBrightness: 0.5,
      imageUrl: 'https://images.unsplash.com/photo-1613182749475-09d63e8001ba?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=967&q=80',
      imageTranslateX: 65,
      imageBlur: 3,
      filter: null
    }
  },
  {
    id: '1',
    userId: '0',
    discoveriesNumber: 0,
    track: {
      title: 'Preben Goes to Acapulco',
      artistName: 'Todd Terje',
      coverImage: 'https://i.scdn.co/image/ab67616d00001e02e92e2ccb32e1b3e58766bd93',
      previewUrl: 'https://p.scdn.co/mp3-preview/b3757ec1ba913ec2b77b8c6d29b42d028e5d40ed?cid=01b3c0340b43402eae3e2845471075cd'
    },
    words: ['Bal', 'Orchestre', 'Fonds Marins'],
    visual: {
      hasCover: true,
      color: '#000',
      colorBrightness: 0.5,
      imageUrl: 'https://images.unsplash.com/photo-1580019542155-247062e19ce4?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=564&q=80',
      imageTranslateX: 50,
      imageBlur: 3,
      filter: null
    }
  },
  {
    id: '2',
    userId: '0',
    discoveriesNumber: 0,
    track: {
      title: 'Tomorrow\'s Dust',
      artistName: 'Tame Impala',
      coverImage: 'https://i.scdn.co/image/ab67616d00001e0258267bd34420a00d5cf83a49',
      previewUrl: 'https://p.scdn.co/mp3-preview/51cb9324908f620a04513107fc69cf10366b5693?cid=774b29d4f13844c495f206cafdad9c86'
    },
    words: ['Passé', 'Nature', 'Secret'],
    visual: {
      hasCover: true,
      color: '#000',
      colorBrightness: 0.5,
      imageUrl: 'https://images.unsplash.com/photo-1601703149866-a33416ff836f?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80',
      imageTranslateX: 50,
      imageBlur: 3,
      filter: null
    }
  },
  {
    id: '3',
    userId: '1',
    discoveriesNumber: 0,
    track: {
      title: 'Rien d\'spécial',
      artistName: 'Népal',
      coverImage: 'https://i.scdn.co/image/ab67616d00001e02dfb3f3ea8b3894c6e4633c14',
      previewUrl: 'https://p.scdn.co/mp3-preview/5e7047566ab42eef32e62951c6fd0c124a4ea638?cid=01b3c0340b43402eae3e2845471075cd'
    },
    words: ['Doute', 'Gouffre', 'Air'],
    visual: {
      hasCover: true,
      color: '#12DDB4',
      colorBrightness: 0.5,
      imageUrl: null,
      imageTranslateX: 0,
      imageBlur: 0,
      filter: null
    }
  },
  {
    id: '4',
    userId: '0',
    discoveriesNumber: 0,
    track: {
      title: '10 & 2',
      artistName: 'NanaBcool',
      coverImage: 'https://i.scdn.co/image/ab67616d00001e021ace24182b5526948aa4d50f',
      previewUrl: 'https://p.scdn.co/mp3-preview/f00263594a8b953b2a806c518b982eb2a7186286?cid=01b3c0340b43402eae3e2845471075cd'
    },
    words: ['Combat', 'Tigre', 'Protestation'],
    visual: {
      hasCover: false,
      color: '#000',
      colorBrightness: 0.5,
      imageUrl: 'https://images.unsplash.com/photo-1500635523027-2f05e513f066?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1022&q=80',
      imageTranslateX: 50,
      imageBlur: 0,
      filter: null
    }
  },
  {
    id: '5',
    userId: '0',
    discoveriesNumber: 0,
    track: {
      title: 'Fata Fou',
      artistName: 'Marc Melià',
      coverImage: 'https://i.scdn.co/image/ab67616d00001e025ceb00c3b8751b87e7f3d8fb',
      previewUrl: 'https://p.scdn.co/mp3-preview/243f40e87a325e888aa4ff87b2b975cd8ab5f8dd?cid=01b3c0340b43402eae3e2845471075cd'
    },
    words: ['Ascension', 'Ciel', 'Envol'],
    visual: {
      hasCover: false,
      color: '#000',
      colorBrightness: 0.5,
      imageUrl: 'https://images.unsplash.com/photo-1473177104440-ffee2f376098?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80',
      imageTranslateX: 50,
      imageBlur: 0,
      filter: null
    }
  },
  {
    id: '6',
    userId: '0',
    discoveriesNumber: 0,
    track: {
      title: 'Seven Times',
      artistName: 'Lianne La Havas',
      coverImage: 'https://i.scdn.co/image/ab67616d00001e02389e657f84c49346d6bdc40d',
      previewUrl: 'https://p.scdn.co/mp3-preview/b32658045ac5bb420704eaafe8b5c5d1bcf8a4f1?cid=01b3c0340b43402eae3e2845471075cd'
    },
    words: ['Confiance', 'Force', 'Danse'],
    visual: {
      hasCover: true,
      color: '#845B23',
      colorBrightness: 0.5,
      imageUrl: null,
      imageTranslateX: 0,
      imageBlur: 0,
      filter: null
    }
  },
  {
    id: '7',
    userId: '0',
    discoveriesNumber: 0,
    track: {
      title: 'L.I.N.E.',
      artistName: 'Kelly Lee Owens',
      coverImage: 'https://i.scdn.co/image/ab67616d00001e02b77946b57299698e3ef1a6ee',
      previewUrl: 'https://p.scdn.co/mp3-preview/409c50851deef04bc859182161eaa0725c6a4150?cid=01b3c0340b43402eae3e2845471075cd'
    },
    words: ['Nuage', 'Rêve', 'Planer'],
    visual: {
      hasCover: true,
      color: '#000',
      colorBrightness: 0.5,
      imageUrl: 'https://images.unsplash.com/photo-1536514498073-50e69d39c6cf?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1051&q=80',
      imageTranslateX: 50,
      imageBlur: 0,
      filter: null
    }
  },
  {
    id: '8',
    userId: '0',
    discoveriesNumber: 0,
    track: {
      title: 'Brutalisme',
      artistName: 'Flavien Berger',
      coverImage: 'https://i.scdn.co/image/ab67616d00001e02b3f0ef2d0288f2dcb2f79830',
      previewUrl: 'https://p.scdn.co/mp3-preview/7c0cc1e8451a097c70976b124105f6b1e79472df?cid=01b3c0340b43402eae3e2845471075cd'
    },
    words: ['Plage', 'Distance', 'Nuit'],
    visual: {
      hasCover: true,
      color: '#000',
      colorBrightness: 0.5,
      imageUrl: 'https://images.unsplash.com/photo-1533903345306-15d1c30952de?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=652&q=80',
      imageTranslateX: 50,
      imageBlur: 0,
      filter: null
    }
  },
  {
    id: '9',
    userId: '0',
    discoveriesNumber: 0,
    track: {
      title: 'Voyager',
      artistName: 'Daft Punk',
      coverImage: 'https://i.scdn.co/image/ab67616d00001e028b56fd8fb9f486c7ebd2303a',
      previewUrl: 'https://p.scdn.co/mp3-preview/48d1a1cb5f02271cad36696f1686a95bf9884e21?cid=01b3c0340b43402eae3e2845471075cd'
    },
    words: ['Route', 'Néon', 'Tunnel'],
    visual: {
      hasCover: true,
      color: '#000',
      colorBrightness: 0.5,
      imageUrl: 'https://images.unsplash.com/photo-1591711584791-455de896b1e9?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80',
      imageTranslateX: 50,
      imageBlur: 0,
      filter: null
    }
  },
  {
    id: '10',
    userId: '0',
    discoveriesNumber: 0,
    track: {
      title: 'Hard to Explain',
      artistName: 'Aleksandir',
      coverImage: 'https://i.scdn.co/image/ab67616d00001e024473d211721486fa3c31cb54',
      previewUrl: 'https://p.scdn.co/mp3-preview/67ca313ad2c425503e9c1d2a6f9df6c9e15824cf?cid=01b3c0340b43402eae3e2845471075cd'
    },
    words: ['Murmure', 'Flou', 'Brume'],
    visual: {
      hasCover: false,
      color: '#000',
      colorBrightness: 0.5,
      imageUrl: 'https://images.unsplash.com/photo-1497864979123-ef3595423b92?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80',
      imageTranslateX: 50,
      imageBlur: 0,
      filter: null
    }
  },
  {
    id: '11',
    userId: '0',
    discoveriesNumber: 0,
    track: {
      title: 'Alone in Kyoto',
      artistName: 'Air',
      coverImage: 'https://i.scdn.co/image/ab67616d00001e026400fab74f28e90759ac8815',
      previewUrl: 'https://p.scdn.co/mp3-preview/c82726e70bfe4915a3c6ab8a5e856d78923c11db?cid=01b3c0340b43402eae3e2845471075cd'
    },
    words: ['Japon', 'Après-midi', 'Brise'],
    visual: {
      hasCover: false,
      color: '#000',
      colorBrightness: 0.5,
      imageUrl: 'https://images.unsplash.com/photo-1602425165569-98a2ea215e6a?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80',
      imageTranslateX: 50,
      imageBlur: 0,
      filter: null
    }
  }
]
