import request from './../request'

export default {
  getTrack (id) {
    return request.get(`tracks/${id}`)
  }
}
