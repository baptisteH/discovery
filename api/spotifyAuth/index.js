import axios from 'axios'

const clientKey = 'MDFiM2MwMzQwYjQzNDAyZWFlM2UyODQ1NDcxMDc1Y2Q6NWNiNTQ1YTM5NDEyNDEyMjkxZTJiMjE2NmIyMzM2YmI='

export default {
  getSpotifyAuthToken () {
    return axios({
      method: 'post',
      url: 'https://accounts.spotify.com/api/token',
      params: {
        grant_type: 'client_credentials'
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Authorization: 'Basic ' + clientKey
      }
    })
  }
}
