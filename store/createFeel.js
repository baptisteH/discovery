export const state = () => ({
  feelInProgress: {
    music: null,
    words: null,
    options: {
      hasCover: false,
      color: '#0aa',
      colorBrightness: 0.5,
      image: null,
      imageTranslateX: 0,
      imageBlur: 0,
      filter: null
    }
  }
})
