export const state = () => ({
  discoveries: []
})

export const mutations = {
  toggleDiscovery (state, { id }) {
    // If discovery not already registered, add it
    if (!state.discoveries.includes(id)) {
      state.discoveries.push(id)
    } else {
      // If discovery already registered, remove it
      const index = state.discoveries.indexOf(id)
      state.discoveries.splice(index, 1)
    }
  }
}

export const getters = {
  getDiscoveries (state) {
    return state.discoveries
  }
}
