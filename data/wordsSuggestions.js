export const wordsSuggestions = [
  {
    icon: '',
    name: 'Paysage',
    words: [
      'Océan', 'Rivière', 'Lac', 'Désert', 'Île', 'Montagne', 'Forêt', 'Plaine', 'Ciel', 'Espace'
    ]
  },
  {
    icon: '',
    name: 'Lieu',
    words: [
      'Maison', 'Campagne', 'Ville', 'Parc', 'Route', 'Rue', 'Urbain'
    ]
  },
  {
    icon: '',
    name: 'Temps',
    words: [
      'Passé', 'Présent', 'Futur', 'Jour', 'Nuit', 'Matin', 'Après-midi', 'Soir'
    ]
  },
  {
    icon: '',
    name: 'Émotion',
    words: [
      'Joie', 'Amour', 'Calme', 'Sérénité', 'Tristesse', 'Nostalgie', 'Colère', 'Peur'
    ]
  },
  {
    icon: '',
    name: 'Éléments',
    words: [
      'Eau', 'Terre', 'Feu', 'Air', 'Ténèbres', 'Lumière'
    ]
  },
  {
    icon: '',
    name: 'Météo',
    words: [
      'Soleil', 'Pluie', 'Vent', 'Neige', 'Grêle', 'Tornade', 'Ouragan'
    ]
  },
  {
    icon: '',
    name: 'Nature',
    words: [
      'Fleur', 'Arbre', 'Plante', 'Fruit', 'Légume'
    ]
  },
  {
    icon: '',
    name: 'Température',
    words: [
      'Chaud', 'Froid', 'Brûlant', 'Glacé'
    ]
  },
  {
    icon: '',
    name: 'Saison',
    words: [
      'Printemps', 'Été', 'Automne', 'Hiver'
    ]
  },
  {
    icon: '',
    name: 'Goût',
    words: [
      'Sucré', 'Salé', 'Amer', 'Acide', 'Epicé',
    ]
  },
  {
    icon: '',
    name: 'Matériau',
    words: [
      'Bois', 'Métal', 'Verre', 'Papier', 'Tissu', 'Plastique'
    ]
  }
]
